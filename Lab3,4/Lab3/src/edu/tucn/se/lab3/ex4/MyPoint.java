package edu.tucn.se.lab3.ex4;
import java.lang.Math;

public class MyPoint {
	private int x;
    private int y;
    
    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setx(int x) {
        this.x = x;
    }

    public int getx() {
        return this.x;
    }

    public void sety(int y) {
        this.y = y;
    }

    public int gety() {
        return this.y;
    }

    public void setxy(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void ToString() {
        System.out.println("(" + this.getx() + ", " + this.gety() + ")" + "\r\n");
    }

    public double distance(int x, int y) {
        return Math.sqrt((this.getx()- x)*(this.getx()-x)+((this.gety() - y)*(this.gety()-y)));
    }

    public double distance(MyPoint another) {
        return Math.sqrt(Math.pow(this.getx() - another.getx(), 2) + Math.pow((this.gety() - another.gety()), 2));
    }
}

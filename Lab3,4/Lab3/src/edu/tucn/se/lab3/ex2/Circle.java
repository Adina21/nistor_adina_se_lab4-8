package edu.tucn.se.lab3.ex2;

public class Circle {
	   static double pi = 3.14159;
	    private double radius;
	    private String color;
	    
	    public Circle() {
	        radius = 1.0;
	        color = "red";
	    }

	    public Circle(double radius) {
	        this.radius = radius;
	        color = "red";
	    }

	    public Circle(String color) {
	        this.color = color;
	        radius = 1.0;
	    }

	    public double getRadius() {
	        return this.radius;
	    }

	    public double getArea() {
	        return this.radius * this.radius * pi;
	    }

	    public void Tostring() {
	        System.out.println("The circle has : ");
	        System.out.println("The radius : " + this.radius);
	        System.out.println("The color: " + this.color);
	        System.out.println("\r\n");

	    }
}

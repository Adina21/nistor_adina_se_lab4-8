package edu.tucn.se.lab3.ex1;

public class Robot {
    private int x;

    public Robot() {
        this.x = 1;
    }

    public void change(int k) {
        if (k >= 1) this.x = k;
        else System.out.println("The value should be greater or equal with one!");
    }

    public void Tostring() {
        System.out.println("The robot position is " + this.x);
    }
    
    public int getx()
    {
        return this.x;
    }
}

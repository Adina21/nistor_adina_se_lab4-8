package edu.tucn.se.lab3.ex3;

public class TestAuthor {
	   public static void main(String[] args) {
	        ///Three authors + functions + print
	        Author Ariana = new Author("Ariana", "g@mail", 'f');
	        Author Melissa = new Author("Melissa", "mss@mail", 'f');
	        Author George = new Author("George", "gg@mail", 'm');

	        System.out.println("The initial values : \r\n");
	        Ariana.ToString();
	        Melissa.ToString();
	        George.ToString();

	        System.out.println("The changed values : \r\n");

	        Ariana.Set_Email("arianaa@gmail.com");
	        Melissa.Set_Gender('f');
	        George.Set_Name("George");

	        Ariana.ToString();
	        Melissa.ToString();
	        George.ToString();
	    }
}

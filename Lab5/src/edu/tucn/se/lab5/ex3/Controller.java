package edu.tucn.se.lab5.ex3;

public class Controller {
	private TemperatureSensor tmps;
	private LightSensor ls;
	public Controller(TemperatureSensor tmpsen,LightSensor lsen)
	{
		tmps=tmpsen;
		ls=lsen;
	}
	public void control()
	{
		int i=1;
		while(i<=20)
		{
			tmps.getValue();
			System.out.println("Temperature location:"+tmps.getLocation()+"second"+i+":"+tmps.readValue());
			ls.getValue();
			System.out.println("Light location"+ls.getLocation()+"second"+i+":"+ls.readValue());
			System.out.println("r/n/");
			i++;
		}
	}
}

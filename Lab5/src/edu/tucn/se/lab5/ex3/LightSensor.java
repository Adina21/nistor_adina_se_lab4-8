package edu.tucn.se.lab5.ex3;

import java.util.Random;

public class LightSensor extends Sensor {
	private int value;
	public LightSensor(String location)
	{
		super(location);
	}
	public void getValue()
	{
		Random rand = new Random();
		this.value= rand.nextInt(100);
	}
	public int readValue()
	{
		return this.value;
	}
}

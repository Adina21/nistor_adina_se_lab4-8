package edu.tucn.se.lab5.ex3;

public class Test {
	public static void main(String[] args) 
	{
        LightSensor ls = new LightSensor("Kitchen");
        TemperatureSensor ts = new TemperatureSensor("Living Room");
        Controller cnt = new Controller(ts, ls);
        cnt.control();
    }
}
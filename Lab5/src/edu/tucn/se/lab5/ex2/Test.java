package edu.tucn.se.lab5.ex2;

public class Test {
	    public static void main(String[] args){
	        RealImage ri = new RealImage("realimg");
	        ri.display();
	        ri.RotatedImage();
	        System.out.println("\r\n");
	        ProxyImage ip=new ProxyImage("proxyimg",'R');
	        System.out.println("\r\n");
	        ip.display();
	        ip.RotatedImage();
	    }	
}

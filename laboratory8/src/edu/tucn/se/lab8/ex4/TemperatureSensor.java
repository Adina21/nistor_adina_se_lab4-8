package edu.tucn.se.lab8.ex4;

public class TemperatureSensor {
    private CoolingSystem cools;
    private HeatingUnit heats;
    public int temperature;
    private int id;
    public TemperatureSensor(int Id, CoolingSystem c, HeatingUnit h) {
        this.id = Id;
        this.cools = c;
        this.heats = h;
    }
    public CoolingSystem getCooling() {
        return this.cools;
    }
    public HeatingUnit getHeats() {
        return this.heats;
    }
}

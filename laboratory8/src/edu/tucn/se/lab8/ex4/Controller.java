package edu.tucn.se.lab8.ex4;
import java.util.ArrayList;
import java.util.Random;

public class Controller {
    private static Controller ContUnit = null;
    private TemperatureSensor TemperatureSensor;
    private ArrayList<FireSensor> fireList = new ArrayList<FireSensor>();
    private Random r = new Random();

    private Controller() {
        TemperatureSensor = new TemperatureSensor(11, new CoolingSystem(), new HeatingUnit());
        FireSensor f1 = new FireSensor(0, new GsmUnit(), new Alarm());
        FireSensor f2 = new FireSensor(1, new GsmUnit(), new Alarm());
        FireSensor f3 = new FireSensor(2, new GsmUnit(), new Alarm());
        FireSensor f4 = new FireSensor(3, new GsmUnit(), new Alarm());
        fireList.add(f1);
        fireList.add(f2);
        fireList.add(f3);
        fireList.add(f4);
    }

    public static Controller getController() {
        if (ContUnit != null) return ContUnit;
        else return new Controller();
    }
    public void control(int event) {
        if (event < 20) {
            TemperatureSensor.temperature = r.nextInt(60);
            System.out.println("The temperature is: "+TemperatureSensor.temperature);
            if (TemperatureSensor.temperature < 25) {
                TemperatureSensor.getHeats().tooCold();
            }
            if (TemperatureSensor.temperature > 40) {
                TemperatureSensor.getCooling().tooHot();
            }
            if(TemperatureSensor.temperature>=25 && TemperatureSensor.temperature<=40){
                System.out.println("The temperature is optimal!");
            }
            System.out.println("\r\n");
        }
        if (event >= 20 && event <= 40) {
            int location = r.nextInt(3);
            fireList.get(location).SetSmoke(r.nextBoolean());
            if (fireList.get(location).GetSmoke() == true) {
                System.out.println("Smoke detected in area covered by the sensor with id" + fireList.get(location).GetId());
                fireList.get(location).GetAlarms().alarmFire();
                fireList.get(location).GetGsm().callOwner();
            } else {
                System.out.println("There is no significant smoke detected (false alarm)!");
            }
            System.out.println("\r\n");
        }
        if (event > 40) {
            System.out.println("The house is OK!");
            System.out.println("\r\n");
        }
    }
}

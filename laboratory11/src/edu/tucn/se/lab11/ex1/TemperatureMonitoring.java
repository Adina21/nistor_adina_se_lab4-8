package edu.tucn.se.lab11.ex1;
import java.awt.BorderLayout;
import javax.swing.*;

public class TemperatureMonitoring extends JFrame{

    TemperatureMonitoring(TemperatureTextView tview){
        setLayout(new BorderLayout());
        add(tview,BorderLayout.NORTH);
        pack();
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        TemperatureSensor t = new TemperatureSensor(); //Starts the temperature sensor
        t.start();

        TemperatureTextView tview = new TemperatureTextView();
        TemperatureController tcontroler = new TemperatureController(t,tview);

        new TemperatureMonitoring(tview);
    }
}

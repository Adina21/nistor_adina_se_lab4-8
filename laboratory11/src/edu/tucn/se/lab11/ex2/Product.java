package edu.tucn.se.lab11.ex2;

public class Product {
    String name;
    int quantity;
    double price;
    public Product(String name, int quantity, double price) {
        this.name=name;
        this.quantity=quantity;
        this.price=price;
    }

    public String getName() {
        return name;
    }
    public int getQuantity() {
        return quantity;
    }
    public double getPrice() {
        return price;
    }
}

package edu.tucn.se.lab11.ex2;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Controller extends JFrame{
	    public ArrayList<Product> list = new ArrayList<>();

	    private JButton add_button = new JButton("Add");
	    private JButton delete_button = new JButton("Delete");
	    private JButton update_button = new JButton("Modify quantity");
	    private JTextArea textArea = new JTextArea();

	    Controller() {
	        super("Stock Management");
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        init();
	        setSize(540, 400);
	        setVisible(true);
	    }

	    public void init() {
	        this.setLayout(null);
	        add_button.setBounds(10, 10, 70, 40);
	        add_button.addActionListener(new AddProduct());
	        delete_button.setBounds(80, 10, 70, 40);
	        delete_button.addActionListener(new DeleteProduct());
	        update_button.setBounds(150, 10, 150, 40);
	        update_button.addActionListener(new Update_Product());
	        textArea.setBounds(10,60,500,300);

	        add(add_button);
	        add(delete_button);
	        add(update_button);
	        add(textArea);

	    }

	    class AddProduct implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            add_frame s = new add_frame(list, textArea);
	            s.setVisible(true);
	        }
	    }

	    class DeleteProduct implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            if (list.isEmpty()) {
	                JOptionPane.showMessageDialog(null, "No products");
	            } else {
	                delete_frame s2 = new delete_frame(list,textArea);
	                s2.setVisible(true);
	            }
	        }
	    }

	    class Update_Product implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            if (list.isEmpty()) {
	                JOptionPane.showMessageDialog(null, "No products");
	            } else {
	                update_frame s3 = new update_frame(list,textArea);
	                s3.setVisible(true);
	            }
	        }
	    }

	}

	class add_frame extends JFrame {
	    private JLabel label_name = new JLabel("Product:");
	    private JLabel label_quantity = new JLabel("Quantity:");
	    private JLabel label_price = new JLabel("Price:");
	    private JTextField productName = new JTextField();
	    private JTextField productQuantity = new JTextField();
	    private JTextField productPrice = new JTextField();
	    private JButton add_button = new JButton("Add");
	    public ArrayList<Product> list;
	    private JTextArea jTextArea;
	    public add_frame(ArrayList<Product> list, JTextArea j) {
	        super("Add Product");
	        this.list = list;
	        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        setSize(450, 400);
	        initAdd();
	        setVisible(true);
	        setLocationRelativeTo(null);
	        this.jTextArea=j;
	    }
	    public void initAdd() {
	        this.setLayout(null);
	        label_name.setBounds(140, 50, 50, 20);
	        productName.setBounds(200, 50, 200, 20);

	        label_quantity.setBounds(140, 200, 50, 20);
	        productQuantity.setBounds(200, 200, 200, 20);

	        label_price.setBounds(140, 120, 50, 20);
	        productPrice.setBounds(200, 120, 200, 20);

	        add_button.setBounds(160, 270, 100, 40);
	        add_button.addActionListener(new Add_Action());


	        add(label_name);
	        add(productName);
	        add(label_quantity);
	        add(productQuantity);
	        add(label_price);
	        add(productPrice);
	        add(add_button);

	    }
	    class Add_Action implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            String Name;
	            int Quantity;
	            double Price;
	            Name = productName.getText();
	            Quantity = Integer.parseInt(productQuantity.getText()); 
	            Price = Double.parseDouble(productPrice.getText());
	            list.add(new Product(Name, Quantity, Price)); 
	            productName.setText("");
	            productQuantity.setText(""); 
	            productPrice.setText("");
	            jTextArea.setText("");
	            for(Product p:list)
	            {
	                jTextArea.append("Product name:"+p.getName()+" price:"+p.getPrice()+" quantity:"+p.getQuantity());
	                jTextArea.append("\n");
	            }
	        }
	    }
	}
	class delete_frame extends JFrame {
	    public ArrayList<Product> list;

	    private JLabel name_label = new JLabel("Product:");
	    private JTextField productName = new JTextField(100);
	    private JButton delete_button = new JButton("Delete");
	    private JTextArea textArea;
	    public delete_frame(ArrayList<Product> list, JTextArea textArea) {
	        super("Delete product");
	        this.list = list;
	        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        setSize(450, 400);
	        initDelete();
	        setVisible(true);
	        this.textArea=textArea;
	        setLocationRelativeTo(null);
	    }
	    public void initDelete() {
	        this.setLayout(null);
	        name_label.setBounds(140, 50, 50, 20);
	        productName.setBounds(200, 50, 200, 20);
	        delete_button.setBounds(160, 270, 100, 40);
	        delete_button.addActionListener(new Delete_Action());

	        add(name_label);
	        add(productName);
	        add(delete_button);
	    }
	    class Delete_Action implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            String Name;
	            Name = productName.getText();
	            for (int i = 0; i < list.size(); i++) {
	                if (list.get(i).name.equals(Name)) {
	                    list.remove(i);
	                } else JOptionPane.showMessageDialog(null, "Product not found!");
	            }
	            productName.setText("");
	            textArea.setText("");
	            for(Product p:list)
	            {
	                textArea.append("Product name:"+p.getName()+" price:"+p.getPrice()+" quantity:"+p.getQuantity());
	                textArea.append("\n");
	            }
	        }
	    }
	}

	class update_frame extends JFrame {
	    public ArrayList<Product> list;

	    private JLabel name_label = new JLabel("Product:");
	    private JTextField productName = new JTextField();
	    private JLabel quantity_label = new JLabel("New quantity:");
	    private JTextField productQuantity = new JTextField();
	    private JButton update_button = new JButton("Update");
	    private JTextArea textArea;

	    public update_frame(ArrayList<Product> list, JTextArea jTextArea) {
	        super("Update quantity");
	        this.list = list;
	        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        setSize(450, 400);
	        initChangeQuantity();
	        setVisible(true);
	        this.textArea=jTextArea;
	        setLocationRelativeTo(null);
	    }

	    public void initChangeQuantity() {
	        this.setLayout(null);
	        name_label.setBounds(140, 50, 50, 20);
	        productName.setBounds(200, 50, 200, 20);

	        quantity_label.setBounds(110, 120, 80, 20);
	        productQuantity.setBounds(200, 120, 200, 20);

	        update_button.setBounds(160, 270, 100, 40);
	        update_button.addActionListener(new Update_Action());

	        add(name_label);
	        add(productName);
	        add(quantity_label);
	        add(productQuantity);
	        add(update_button);
	    }

	    class Update_Action implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            String Name;
	            int Quantity;
	            Name = productName.getText();   //Looks for the name and change quantity with the new value or error
	            Quantity = Integer.parseInt(productQuantity.getText());
	            for (Product product : list) {
	                if (product.name.equals(Name)) {
	                    product.quantity = Quantity;
	                } else JOptionPane.showMessageDialog(null, "Product not found!");
	            }
	            productName.setText("");
	            productQuantity.setText(""); //Clear the textfields plus update list
	            textArea.setText("");
	            for(Product p:list)
	            {
	                textArea.append("Product name:"+p.getName()+" price:"+p.getPrice()+" quantity:"+p.getQuantity());
	                textArea.append("\n");
	            }
	        }
	    }
}

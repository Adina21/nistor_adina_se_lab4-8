package edu.tucn.se.lab6.ex3;
import edu.tucn.se.lab6.ex1.BankAccount;
import java.util.TreeSet;
public class BankTreeSet {
	  private   TreeSet<BankAccount> tbalance = new TreeSet<BankAccount>();
	    private   TreeSet<BankAccount> towner = new TreeSet<BankAccount>();
	    public BankTreeSet(TreeSet<BankAccount> acc,TreeSet<BankAccount> own) {
	        this.tbalance = acc;
	        this.towner = own;
	    }
	    public void addAccount(String Owner, double Balance) {
	        BankAccount anew = new BankAccount(Owner, Balance);
	        tbalance.add(anew);
	        towner.add(anew);
	    }
	    public void printAccountsByOwners() {
	        ///Sorting the array by balance + print
	        System.out.println("\r\nSorted Alphabetically: ");
	        for (BankAccount ba : towner)
	            System.out.println(ba.toString());
	        System.out.println("\r\n");
	    }
	    public void printAccounts(double minBalance, double maxBalance) {
	        for (BankAccount ba : tbalance) {
	            if (ba.getBalance() > minBalance && ba.getBalance() < maxBalance) System.out.println(ba.toString());
	        }
	    }
	    public BankAccount getAccount(String own) {
	        for (BankAccount ba : tbalance) {
	            if (ba.getOwner() == own) return ba;
	        }
	        return new BankAccount("NU EXISTA CONT",0);
	    }
	    public void printAccountsByBalance(){
	        System.out.println("\r\n Sorted by balance: ");
	        for (BankAccount ba : tbalance)
	            System.out.println(ba.toString());
	        System.out.println("\r\n");
	    }
}

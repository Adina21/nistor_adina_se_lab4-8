package edu.tucn.se.lab6.ex3;
import edu.tucn.se.lab6.ex1.BankAccount;
import edu.tucn.se.lab6.ex2.SortByBalance;
import edu.tucn.se.lab6.ex2.SortByOwner;
import java.util.TreeSet;

public class TestBATreeSet {
    public static void main(String[] args) {
        TreeSet<BankAccount> tb = new TreeSet<BankAccount>(new SortByBalance());
        TreeSet<BankAccount> to = new TreeSet<BankAccount>(new SortByOwner());
        BankAccount ba1 = new BankAccount("Dan", 21567);
        BankAccount ba2 = new BankAccount("Ana", 2321);
        BankAccount ba3 = new BankAccount("Andrei", 230);
        BankAccount ba4 = new BankAccount("Cristina", 6500);
        BankAccount ba5 = new BankAccount("Patricia", 29);
        BankAccount ba6 = new BankAccount("Sebastian", 3500);
        tb.add(ba1);
        tb.add(ba2);
        tb.add(ba3);
        tb.add(ba4);
        tb.add(ba5);
        tb.add(ba6);

        to.add(ba1);
        to.add(ba2);
        to.add(ba3);
        to.add(ba4);
        to.add(ba5);
        to.add(ba6);

        BankTreeSet vara = new BankTreeSet(tb,to);
        vara.printAccountsByBalance();
        vara.printAccountsByOwners();

        vara.addAccount("Alex",2345);
        System.out.println("Array with Iosif added sorted by Owner's name alphabetically : \r\n");
        vara.printAccountsByOwners();

        System.out.println("\r\nGive Dan Account: ");
        BankAccount cont = vara.getAccount("Dan");
        System.out.println("Dan account is :"+cont.toString());

        System.out.println("\r\nGive all accounts between 100 and 3000:");
        vara.printAccounts(100,3000);
    }
}

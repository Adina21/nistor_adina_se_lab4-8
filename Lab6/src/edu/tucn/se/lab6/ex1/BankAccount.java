package edu.tucn.se.lab6.ex1;

public class BankAccount {
	private String owner;
	private double balance;
	public BankAccount(String owner,double balance)
	{
		this.owner=owner;
		this.balance=balance;
	}
	public void withdraw(double amount)
	{
		this.balance=this.balance-amount;
	}
	public void deposit(double amount)
	{
		this.balance=this.balance+amount;
	}	
    public String getOwner() {
        return this.owner;
    }
    public double getBalance() {
        return this.balance;
    }
    public boolean equals(Object ba){
        if(ba==null||!(ba instanceof BankAccount) )
            return false;
        else
        {
        	BankAccount x = (BankAccount)ba;
        	return x.owner==owner&&x.balance==balance;
        }
    }
 
    public int hashCode(){
        return owner.hashCode()+(int)balance;
    }
 
    public String toString(){
        return "The bank account belongs to : " + this.owner + " and it has stored in it : " + this.balance + " $" + "\r\n";
    }
}

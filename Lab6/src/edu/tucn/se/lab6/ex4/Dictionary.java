package edu.tucn.se.lab6.ex4;

public class Dictionary {
    private static StoreWord dictionary;
    public Dictionary(StoreWord st) {
        this.dictionary = st;
    }
    public void addWord(Word wd, Definition def) {
        dictionary.dict.put(wd, def);
    }

    public Definition getDefinition(Word wd) {
        for (Word j : dictionary.dict.keySet()) {
            if (j.equals(wd)){
                return dictionary.dict.get(j);
            }
        }
      return null;
    }
    public void getAllWords() {
        for (Word i : dictionary.dict.keySet()) {
            System.out.println(i.toString() + "\r\n");
        }
    }
    public void getAll() {
        for (Word i : dictionary.dict.keySet()) {
            System.out.println(i.toString() +":  "+dictionary.dict.get(i).toString()+"\r\n");
        }
    }

    public void getAllDefinitions() {
        for ( Word j: dictionary.dict.keySet() ) {
            System.out.println( dictionary.dict.get(j).toString() + "\r\n");
        }
    }
}


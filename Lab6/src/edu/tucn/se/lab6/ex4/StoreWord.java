package edu.tucn.se.lab6.ex4;
import java.util.HashMap;

public class StoreWord {
    public static HashMap<Word, Definition> dict = new HashMap<Word, Definition>();
    public StoreWord(Word wd, Definition def) {
        dict.put(wd, def);
    }
    public StoreWord(){}
}

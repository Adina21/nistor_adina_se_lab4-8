package edu.tucn.se.lab4.ex5;
import edu.tucn.se.lab4.ex1.Circle;;
 
public class Cylinder extends Circle{
	private double height=1.0;
	
	public Cylinder()
	{
		super();
	}
	public Cylinder(double radius)
	{
		super(radius);
	}
	public Cylinder(double radius,double height)
	{
		super(radius);
		this.height=height;
	}
	public double getHeight()
	{
		return height;
	}
	@Override
    public double getArea() {
        return 2*super.getArea()+2*3.14159*this.getRadius()*this.height;
    }
}

package edu.tucn.se.lab4.ex2;

public class TestAuthor {
	public static void main(String[] args) {
        ///Three authors + functions + print
        Author Paul = new Author("Paul", "gp@mail", 'm');
        Author Bogdan = new Author("Bogdan", "ybg@mail", 'f');
        Author Damaris = new Author("Damaris", "dla@mail", 'f');

        System.out.println("The initial values : \r\n");
        Paul.ToString();
        Bogdan.ToString();
        Damaris.ToString();

        System.out.println("The changed values : \r\n");

        Paul.setEmail("dpl@gmail.com");
        Bogdan.setGender('m');
        Damaris.setName("Elena");

        Paul.ToString();
        Bogdan.ToString();
        Damaris.ToString();
    }
}

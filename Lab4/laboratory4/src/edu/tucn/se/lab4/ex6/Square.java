package edu.tucn.se.lab4.ex6;

public class Square extends Rectangle{
	public Square()
	{
		super();
	}
	public Square(double side)
	{
		super(side,side);
	}
	public Square(double side, String color, boolean filled)
	{
		super(side,side,color,filled);
	}
	public double getSide()
	{
		return super.getWidth();
	}
	public void setSide(double side)
	{
		super.setWidth(side);
	}
	@Override
	public void setLength(double side) {
	    super.length = super.width = side;
	}
	@Override
	public void setWidth(double side)
	{
		super.width=super.length=side;
	}
	@Override
	public String toString()
	{
		return "A square with side="+super.length+"which is a subclass of"+super.toString();
	}
}

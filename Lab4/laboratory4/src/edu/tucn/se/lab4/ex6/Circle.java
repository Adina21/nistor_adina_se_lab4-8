package edu.tucn.se.lab4.ex6;

public class Circle extends Shape {
	private double radius=1.0;	
	public Circle()
	{
		super();
		radius=1.0;
	}
	public Circle(double radius)
	{
		super();
		this.radius=radius;
	}
	public Circle(double radius,String color, boolean filled)
	{
		super(color,filled);
		this.radius=radius;	
	}
	public double getRadius()
	{
		return radius;
	}
	public void setRadius(double radius)
	{
		this.radius=radius;
	}
	public double getArea()
	{
		return 3.14*radius*radius;
	}
	public double getPerimeter()
	{
		return 2*3.14*radius;
	}
	@Override
	public String toString()
	{
		return "A circle with radius="+this.radius+",which is a subclass of "+super.toString();
	}
}

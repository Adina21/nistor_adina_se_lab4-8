package edu.tucn.se.lab4.ex3;
import edu.tucn.se.lab4.ex2.Author;

public class TestBook {
	public static void main(String[] args) 
	{
		Author author= new Author("Melissa Marr","cml@gmail.com",'f');
		Book b = new Book("Fascinatie",author,53,150);
		Book bo = new Book("Obsesie",author,64,321);
        System.out.println("Initial values: \r\n");
        author.ToString();
        b.ToString();
        bo.ToString();
	}
}

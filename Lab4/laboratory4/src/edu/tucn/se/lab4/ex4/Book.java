package edu.tucn.se.lab4.ex4;
import edu.tucn.se.lab4.ex2.Author;

public class Book {
	private String name;
	private Author authors[]=new Author[5];
	private double price;
	private int qtyInStock=0;
	
	public Book(String name,Author authors[],double price)
	{
		this.name = name;
		this.authors = authors;
		this.price = price;
	}
	public Book(String name,Author authors[],double price,int qtyInStock)
	{
		this.name = name;
		this.authors = authors;
		this.price = price;
		this.qtyInStock = qtyInStock;
	}
	public String getName()
	{
		return name;
	}
	public Author getAuthorsLength()
	{
		return authors[authors.length];
	}
	public Author[] getAuthors()
	{
		return authors;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price=price;
	}
	public int getQtyInStock()
	{
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	public String toString()
	{
		return "Book [name=" +name + "authors =" + authors[0]+"email = " + authors[0].getEmail() + "price= "+price + "qty = " + qtyInStock + "]";
	}
	
	  public void printAuthors() {
	        for (Author i:authors) {
	            i.ToString();
	            System.out.println("\r\n");
	        }
}
}

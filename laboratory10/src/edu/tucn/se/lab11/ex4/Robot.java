package edu.tucn.se.lab11.ex4;
import javax.swing.*;
import java.awt.*;
import java.util.Random;


public class Robot extends Thread {
	JLabel img;
	 boolean running = true;
	 Point p = new Point();
	 BoundingBox b;
	 Random r = new Random();
	 
	 Robot(){
	 super(); //Creating the robot
	 this.b = new BoundingBox(p, Utils.characterSize);
	 img = new JLabel(new ImageIcon("C:/Users/dalia/Desktop/SE/laboratory10/src/edu/tucn/se/lab11/ex4/robfin.jpg"));
	 // Random point
	 img.setBounds(r.nextInt(Utils.winSize - Utils.characterSize),
	 r.nextInt(Utils.winSize - Utils.characterSize), Utils.characterSize, Utils.characterSize);
	 img.setVisible(true);
	 
	 }
	 public void run(){
	 
	 while(running){
	 //Constantly changing the position of a robot second by second
	 int x = r.nextInt(Utils.winSize - Utils.characterSize);
	 int y = r.nextInt(Utils.winSize - Utils.characterSize);
	 p.setXY(x,y);
	 System.out.println("" + x + "," + y);
	 img.setLocation(p.x, p.y);
	 try {
	 Thread.sleep(1000);
	 } catch (InterruptedException e) {
	 e.printStackTrace();
	 }
	 }
	 }
	 
	 BoundingBox getBoundingBox(){
	 return b;
	 }
	 
	 boolean testCollision(Robot b){
	 return this.getBoundingBox().testCollision(b.getBoundingBox());
	 }
	 //Get the robot
	 JLabel getLabel(){
	 return img;
	 }
	 //Stopping The Thread
	 void stopTh(){
	 img.setVisible(false);
	 running = false;
	 }
}

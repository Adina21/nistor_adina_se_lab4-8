package edu.tucn.se.lab7.ex3;
import java.io.*;
import java.util.Scanner;
import java.util.zip.*;

public class Main {
	 static void encryptFile(String name) {
	        try {
	            BufferedReader input = new BufferedReader(new FileReader(name));
	            String newname = "";
	            int i = 0;
	            while (name.charAt(i) != '.') {
	                newname = newname + name.charAt(i);
	                i++;
	            }
	            newname = newname + ".enc";
	            File file = new File("/Users/dalia/Desktop/SE/laboratory7/src/edu/tucn/se/lab7/ex3/" + newname);
	            BufferedWriter output = new BufferedWriter(new FileWriter(newname));
	            int character;
	            while ((character = input.read()) != -1) {
	                character = character << 1;
	                output.write(character);
	            }
	            input.close();
	            output.close();
	        } catch (IOException e) {
	            System.err.println("I/O error: " + e.getMessage());
	        }
	    }
	   static void decryptFile(String name) {
	        try {
	            BufferedReader input = new BufferedReader(new FileReader(name));
	            String newname = "";
	            int i = 0;
	            while (name.charAt(i) != '.') {
	                newname = newname + name.charAt(i);
	                i++;
	            }
	            newname = newname + ".dec";
	            File file = new File("/Users/dalia/Desktop/SE/laboratory7/src/edu/tucn/se/lab7/ex3/" + newname);
	            BufferedWriter output = new BufferedWriter(new FileWriter(newname));
	            int character;
	            while ((character = input.read()) != -1) {
	                character = character >> 1;
	                output.write((char) character);
	            }
	            input.close();
	            output.close();
	        } catch (IOException e) {
	            System.err.println("I/O error: " + e.getMessage());
	        }
	    }
	public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("\r\nWelcome to the Encrypting Program");
        int opt = 1;
        while (opt != 0) {
            System.out.println("1.Encrypt file");
            System.out.println("2.Decrypt File");
            System.out.println("0.Close");
            System.out.println("Give command:");
            opt = in.nextInt();
            switch (opt) {
                case 1: {
                    System.out.print("\r\nGive the file to encrypt:");
                    String fis = in.next();
                    encryptFile("src/edu/tucn/se/lab7/ex3/" + fis);
                    break;
                }
                case 2: {
                    System.out.print("\r\nGive the file to decrypt:");
                    String fis = in.next();
                    decryptFile("src/edu/tucn/se/lab7/ex3/" + fis);
                    break;
                }
                case 0: {
                    break;
                }
                default: {
                    System.out.println("Invalid command!");
                }
            }

        }
    }
}

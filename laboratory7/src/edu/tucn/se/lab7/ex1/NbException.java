package edu.tucn.se.lab7.ex1;

public class NbException extends Exception{
    private int number;
    public NbException(String message,int nmb) 
    {
    	super(message);
    	this.number=nmb;
    }
    public int getNumber(){
    	return number;
    	}
}

package edu.tucn.se.lab7.ex1;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();
        Cofee.nbcofees = 0;
        for (int i = 0; i < 15; i++) {
            Cofee c = mk.makeCofee();
            try {
                d.drinkCofee(c);
            }
            catch (NbException e) {
                System.out.println("Exception:"+e.getMessage()+ " Number= "+e.getNumber());
            }
            finally {
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}


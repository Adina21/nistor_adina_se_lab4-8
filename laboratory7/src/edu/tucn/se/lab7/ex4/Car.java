package edu.tucn.se.lab7.ex4;
import java.io.Serializable;
public class Car implements Serializable{

    private String model;
    private double price;
    public Car(String mod , double pri){this.model=mod;this.price=pri;}
    public Car(){}
    public void setModel(String mod) {this.model=mod;}
    public void setPrice(double pri) {this.price=pri;}
    public String getModel() {return this.model;}
    public double getPrice() {return this.price;}

}
